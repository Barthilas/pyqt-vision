# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'listItemRow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal, QObject

class listRowView(QtWidgets.QWidget):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(253, 29)
        Form.setMinimumSize(QtCore.QSize(253, 29))
        Form.setMaximumSize(QtCore.QSize(16777215, 38))
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(Form)
        self.horizontalLayout_2.setContentsMargins(3, 3, 3, 3)
        self.horizontalLayout_2.setSpacing(3)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.labelOperation = QtWidgets.QLabel(Form)
        self.labelOperation.setObjectName("labelOperation")
        self.horizontalLayout_2.addWidget(self.labelOperation)
        spacerItem = QtWidgets.QSpacerItem(40, 10, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.buttonDelete = QtWidgets.QPushButton(self.frame)
        self.buttonDelete.setText("")
        self.buttonDelete.setObjectName("buttonDelete")
        self.horizontalLayout.addWidget(self.buttonDelete)
        self.buttonEdit = QtWidgets.QPushButton(self.frame)
        self.buttonEdit.setText("")
        self.buttonEdit.setObjectName("buttonEdit")
        self.horizontalLayout.addWidget(self.buttonEdit)
        self.buttonUp = QtWidgets.QPushButton(self.frame)
        self.buttonUp.setText("")
        self.buttonUp.setObjectName("buttonUp")
        self.horizontalLayout.addWidget(self.buttonUp)
        self.buttonDown = QtWidgets.QPushButton(self.frame)
        self.buttonDown.setText("")
        self.buttonDown.setObjectName("buttonDown")
        self.horizontalLayout.addWidget(self.buttonDown)
        self.horizontalLayout_2.addWidget(self.frame)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.labelOperation.setText(_translate("Form", "TextLabel"))

    ###CUSTOM###
    editSignal = pyqtSignal(object)
    deleteSignal = pyqtSignal()
    upSignal = pyqtSignal()
    downSignal = pyqtSignal()
    def __init__(self, name, parent=None):
        super(listRowView, self).__init__(parent)
        self.setupUi(self)
        self.appendBindings()
        self.appendIcons()
    def appendBindings(self):
        self.buttonEdit.clicked.connect(self.editButtonEvent)
        self.buttonDelete.clicked.connect(self.deleteButtonEvent)
        self.buttonUp.clicked.connect(self.upButtonEvent)
        self.buttonDown.clicked.connect(self.downButtonEvent)

    def appendIcons(self):
        icon_width = 24
        icon_height = 24
        self.buttonDelete.setIcon(QtGui.QIcon('res/delete.png'))
        self.buttonDelete.setIconSize(QtCore.QSize(icon_width,icon_height))
        self.buttonDown.setIcon(QtGui.QIcon('res/arrow_down.png'))
        self.buttonDown.setIconSize(QtCore.QSize(icon_width,icon_height))
        #self.buttonDown.setStyleSheet("background:transparent; border:0px;")
        self.buttonUp.setIcon(QtGui.QIcon('res/arrow_up.png'))
        self.buttonUp.setIconSize(QtCore.QSize(icon_width,icon_height))
        self.buttonEdit.setIcon(QtGui.QIcon('res/edit.png'))
        self.buttonEdit.setIconSize(QtCore.QSize(icon_width,icon_height))

    def editButtonEvent(self):
        self.editSignal.emit(self)
    def deleteButtonEvent(self):
        self.deleteSignal.emit()
    def upButtonEvent(self):
        self.upSignal.emit()
    def downButtonEvent(self):
        self.downSignal.emit()