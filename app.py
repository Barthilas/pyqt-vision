from PyQt5 import QtCore, QtGui, QtWidgets
from view import style
from view import mainWindowView as mainView
from controller import mainWindowController

class Application(QtWidgets.QApplication):
    def __init__(self, argv=None):
        super(Application, self).__init__(argv)
        self.windows = []
        win = mainWindowController.MainWindow(self)
        win.show()
        self.windows.append(win)
        style.setStyle(self)

if __name__ == "__main__":
    import sys
    app = Application(sys.argv)

    sys.exit(app.exec_())