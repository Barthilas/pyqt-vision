import os.path
from multiprocessing import Process
import cv2
class ImageScripts():

    WORKSPACE_PATH = "workspace/"

    @staticmethod
    def checkWorkSpaceDir(path=WORKSPACE_PATH):
        dir = os.path.join(path)
        if not os.path.exists(dir):
            os.mkdir(dir)

    @staticmethod
    def checkPathForDir(path):
        dir = os.path.join(path)
        if not os.path.exists(dir):
            os.mkdir(dir)

    @staticmethod
    def deleteWorkSpaceDir():
        dir = os.path.join(ImageScripts.WORKSPACE_PATH)
        if os.path.exists(dir):
            os.rmdir(dir)

    @staticmethod
    def bgr2ColorSpace(input, colorSpace, saveLocation=None, fileName=None, extension="png"):
        try:
            image = cv2.imread(os.path.join(input))
            transformed = cv2.cvtColor(image, colorSpace)
            
            if saveLocation==None:
                cv2.imshow('rotated',transformed)
            else:
                ImageScripts.checkPathForDir(saveLocation)
                if fileName==None:
                    _p = os.path.join(saveLocation, "colorSpace.{}".format(extension))
                    return _p
                else:
                    fileName += "." + extension
                    _p = os.path.join(saveLocation, fileName)
                    cv2.imwrite(_p, transformed)
                    return _p
            
        except Exception:
            print("Input file of none.")
    
    @staticmethod
    def imageRotate(input, degrees, saveLocation=None, fileName=None, extension="png"):
        normalizedDegree = degrees % 360
        if normalizedDegree==0:
            pass

        img = cv2.imread(input)
        rows,cols,ch = img.shape
        center = (cols/2,rows/2)
        M = cv2.getRotationMatrix2D(center,normalizedDegree,1)
        rotated = cv2.warpAffine(img,M,(cols,rows))

        if saveLocation==None:
            cv2.imshow('rotated',rotated)
        else:
            ImageScripts.checkPathForDir(saveLocation)
            if fileName==None:
                _p = os.path.join(saveLocation, "rotatedBy{}.{}".format(degrees,extension))
            else:
                fileName += "." + extension
                _p = os.path.join(saveLocation, fileName)
            cv2.imwrite(_p, rotated)
        

    @staticmethod
    def imageScalePercentage(input, scale, scaleX=True, scaleY=True, saveLocation=None, fileName=None, extension="png"):
        if scale==100:
            pass

        img = cv2.imread(os.path.join(input))
        _w = img.shape[1]
        _h = img.shape[0]
        width = int(_w * scale/ 100) if scaleX else _w
        height = int(_h * scale / 100) if scaleY else _h
        dim = (width, height)
        interpolation_mode = cv2.INTER_AREA if scale<100 else cv2.INTER_CUBIC
        resized = cv2.resize(img, dim, interpolation = interpolation_mode)

        if saveLocation==None:
            cv2.imshow('resized',resized)
        else:
            ImageScripts.checkPathForDir(saveLocation)
            if fileName==None:
                _p = os.path.join(saveLocation, "resized{}x{}.{}".format(width,height ,extension))
            else:
                fileName += "." + extension
                _p = os.path.join(saveLocation, fileName)
            cv2.imwrite(_p, resized)

    @staticmethod
    def imageScaleExact(input, width, height, saveLocation=None, fileName=None, extension="png"):
        img = cv2.imread(os.path.join(input))
        _w = img.shape[1]
        _h = img.shape[0]

        if(_w == width and _h == height):
            pass

        dim = (width, height)
        interpolation_mode = cv2.INTER_AREA if (width+height)<(_w+_h) else cv2.INTER_CUBIC
        resized = cv2.resize(img, dim, interpolation = interpolation_mode)
        if saveLocation==None:
            cv2.imshow('img',resized)
        else:
            ImageScripts.checkPathForDir(saveLocation)
            if fileName==None:
                _p = os.path.join(saveLocation, "resized{}x{}.{}".format(width,height ,extension))
            else:
                fileName += "." + extension
                _p = os.path.join(saveLocation, fileName)
            cv2.imwrite(_p, resized)

if __name__ == "__main__":
    print("Console mode.")
    img = 'res/lenna.png'
    #res = ImageScripts.imageScalePercentage(img, 80, saveLocation=ImageScripts.WORKSPACE_PATH, fileName="neco")
    #ress = ImageScripts.imageScaleExact(img, 10,10,saveLocation=ImageScripts.WORKSPACE_PATH, fileName="neco")
    lala = ImageScripts.imageRotate(img, 360)

    p = Process(target=ImageScripts.bgr2ColorSpace(img, cv2.COLOR_BGR2GRAY))   
    p.start()
    p.join()
