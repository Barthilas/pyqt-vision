from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from view import mainWindowView
from view import listRowView
from scripts import image_scripts
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
import time
import cv2

class MainWindow(QtWidgets.QMainWindow, mainWindowView.Ui_MainWindow):
   
    fileName = None


    def __init__(self, parentApp, parent=None):
        super(MainWindow, self).__init__(parent)
        self.parentApp : QtWidgets.QApplication
        self.parentApp = parentApp
        self.setupUi(self)
        #Append bindings
        self.bindingsConnection()

    def bindingsConnection(self):
        self.pushButton.clicked.connect(self.vlastniMetoda)
        self.pushButton_2.clicked.connect(self.DeleteSelectedItem)
        self.actionplaceholder1_0.triggered.connect(self.actionClicked)
        self.actionOpen_file.triggered.connect(self.openFileNameDialog)
        self.actionplaceholder1_1.triggered.connect(self.rgb2GrayEvent)
        self.pushButton_3.clicked.connect(self.listWidgetUp)
        self.pushButton_4.clicked.connect(self.longtimeOperationQThread)
        self.actionChange_Colorspace.triggered.connect(self.changeColorSpaceEvent)

    def changeColorSpaceEvent(self):
        def transfer(_f, _t):
            _from = _f
            _to = _t
            conversion = None
            self.mdiArea.closeAllSubWindows()
            self.parentApp.windows.pop(len(self.parentApp.windows)-1)#App no longer knows about this window -> GC
            if(_from == "BGR" and _to == "GRAY"):
                conversion = cv2.COLOR_BGR2GRAY
            path = image_scripts.ImageScripts.bgr2ColorSpace(self.fileName, conversion, image_scripts.ImageScripts.WORKSPACE_PATH, "neco")
            pixmap = QtGui.QPixmap(path)
            ScaledPixmap = pixmap.scaled(self.changedImage.size(), QtCore.Qt.IgnoreAspectRatio)
            self.changedImage.setPixmap(ScaledPixmap)


        from view import colorSpaceDialogView
        dialogWindow = QtWidgets.QWidget()
        dialogWindow.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        
        dialog = colorSpaceDialogView.Ui_Form(self)
        dialog.setupUi(dialogWindow)
        dialog.dialogSuccesSignal.connect(transfer)
        self.mdiArea.addSubWindow(dialogWindow)
        dialogWindow.show()
        #You need to store it somewhere or the GC will collect it.
        self.parentApp.windows.append(dialogWindow)


    def actionClicked(self):
        action = self.ControlsFrame.sender()
        print(action.text())
        self.operationListWidget.addItem(action.text())

    def rgb2GrayEvent(self):
        import cv2
        image_scripts.ImageScripts.bgr2ColorSpace(self.fileName, cv2.COLOR_BGR2GRAY)

    def listWidgetUp(self):
        currentRow = self.operationListWidget.currentRow()
        currentItem = self.operationListWidget.takeItem(currentRow)
        self.operationListWidget.insertItem(currentRow - 1, currentItem)

    def vlastniMetoda(self):
        # Add to list a new item (item is simply an entry in your list)
        item = QtWidgets.QListWidgetItem(self.operationListWidget)
        self.operationListWidget.addItem(item)
        # Instanciate a custom widget 
        row = listRowView.listRowView("bla", parent=self.operationListWidget)

        import random
        row.labelOperation.setText(str(random.random()))
        row.editSignal.connect(self.editSignalResponse)
        row.deleteSignal.connect(self.deleteSignalResponse)
        row.upSignal.connect(self.upSignalResponse)
        row.downSignal.connect(self.downSignalResponse)
        item.setSizeHint(row.minimumSizeHint())

        # Associate the custom widget to the list entry
        self.operationListWidget.setItemWidget(item, row)
    def editSignalResponse(self, obj):
            print(obj)

    def deleteSignalResponse(self):
        ##BLACK MAGIN IN NUTSHELL##
        ##OTHER WAY TO DO THIS: https://stackoverflow.com/questions/54753024/pyqt5-find-row-of-qlistwidgetitem?rq=1"
        sender = self.sender()
        pos = self.operationListWidget.viewport().mapFromGlobal(
        sender.mapToGlobal(QtCore.QPoint(1, 1)))
        item = self.operationListWidget.itemAt(pos)
        if item is not None:
            # delete both the widget and the item
            widget = self.operationListWidget.itemWidget(item)
            self.operationListWidget.removeItemWidget(item)
            row = self.operationListWidget.row(item)
            self.operationListWidget.takeItem(row)
            widget.deleteLater()
    
    def upSignalResponse(self):
        sender = self.sender()
        pos = self.operationListWidget.viewport().mapFromGlobal(
        sender.mapToGlobal(QtCore.QPoint(1, 1)))
        item = self.operationListWidget.itemAt(pos)
        if item is not None:
            row_num = self.operationListWidget.row(item)
            if row_num > 0:
                self.operationListWidget.setCurrentRow(row_num)
                row = self.operationListWidget.itemWidget(self.operationListWidget.currentItem())
                itemN = self.operationListWidget.currentItem().clone()

                self.operationListWidget.insertItem(row_num -1, itemN)
                self.operationListWidget.setItemWidget(itemN, row)

                self.operationListWidget.takeItem(row_num+1)
                self.operationListWidget.setCurrentRow(row_num-1)

    def downSignalResponse(self):
        sender = self.sender()
        pos = self.operationListWidget.viewport().mapFromGlobal(
        sender.mapToGlobal(QtCore.QPoint(1, 1)))
        item = self.operationListWidget.itemAt(pos)
        if item is not None:
            row_num = self.operationListWidget.row(item)
            if row_num == -1:
            # no selection. abort
                return
            elif row_num < self.operationListWidget.count():
                pass
                self.operationListWidget.setCurrentRow(row_num)
                row = self.operationListWidget.itemWidget(self.operationListWidget.currentItem())
                itemN = self.operationListWidget.currentItem().clone()

                self.operationListWidget.insertItem(row_num +2, itemN)
                self.operationListWidget.setItemWidget(itemN, row)

                self.operationListWidget.takeItem(row_num)
                self.operationListWidget.setCurrentRow(row_num+1)

    def DeleteSelectedItem(self):
        listItems=self.operationListWidget.selectedItems()
        if not listItems: return        
        for item in listItems:
                self.operationListWidget.takeItem(self.operationListWidget.row(item))

    
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(caption="QFileDialog.getOpenFileName()", directory="",filter="All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            self.fileName = fileName
            pixmap = QtGui.QPixmap(fileName)
            ScaledPixmap = pixmap.scaled(self.originalImage.size(), QtCore.Qt.IgnoreAspectRatio)
            self.originalImage.setPixmap(ScaledPixmap)

    def longOperationSimulation(self):
        import time
        time.sleep(10) # Sleep for 3 seconds
        print("my time is up.")

    def longtimeOperationThread(self):
        #Qthread vs Thread??
        import threading
        x = threading.Thread(target=self.longOperationSimulation, daemon=True)
        x.start()

    def longtimeOperationQThread(self):

        # 1 - create Worker and Thread inside the Form
       self.obj = Worker()  # no parent!
       self.thread = QThread()  # no parent!
       # 2 - Connect Worker`s Signals to Form method slots to post data.
       self.obj.intReady.connect(self.progressBar.setValue) #or onIntReady
       # 3 - Move the Worker object to the Thread object
       self.obj.moveToThread(self.thread)
       # 4 - Connect Worker Signals to the Thread slots
       self.obj.finished.connect(self.thread.quit)
       # 5 - Connect Thread started signal to Worker operational slot method
       self.thread.started.connect(self.obj.procCounter)
       # * - Thread finished signal will close the app if you want!
       #self.thread.finished.connect(app.exit)
       # 6 - Start the thread
       self.thread.start()

    def onIntReady(self, i):
        self.progressBar.setValue(i)
        #print(i)

# worker.py
class Worker(QObject):
    finished = pyqtSignal()
    intReady = pyqtSignal(int)

    @pyqtSlot()
    def procCounter(self): # A slot takes no params
        for i in range(1, 100):
            time.sleep(1)
            self.intReady.emit(i)

        self.finished.emit()